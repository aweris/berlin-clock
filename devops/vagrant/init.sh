#!/bin/bash

vagrant up

vagrant ssh worker-1 -c "fleetctl start /opt/services/instances/*"

exit 0