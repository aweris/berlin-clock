$update_channel="stable"
$worker_count=3
$worker_vm_memory=1024
$worker_metadata = "environment=development,role=worker"
$etcd_count=1
$etcd_vm_memory=512
$etcd_metadata = "environment=development,role=etcd"
$new_discovery_url="https://discovery.etcd.io/new?size=#{$etcd_count}"