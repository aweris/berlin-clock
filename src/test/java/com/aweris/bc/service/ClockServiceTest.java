package com.aweris.bc.service;

import com.aweris.bc.Application;
import com.aweris.bc.domain.Clock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClockServiceTest {

    @Autowired
    private ClockService clockService;


    @Test
    public void clock_by_time_0_hour_0_minute_0_second() throws Exception {

        LocalTime time = LocalTime.of(0, 0, 0);

        Clock c = clockService.getClock(time);

        assertThat(c.toString()).isEqualTo("Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO\n");
    }

    @Test
    public void clock_by_time_17_hour_36_minute_5_second() throws Exception {

        LocalTime time = LocalTime.of(17, 36, 5);

        Clock c = clockService.getClock(time);

        assertThat(c.toString()).isEqualTo("O\nRRRO\nRROO\nYYRYYRYOOOO\nYOOO\n");
    }

    @Test
    public void clock_by_values_clock_23_hour_59_minute_59_second() throws Exception {

        Clock c = clockService.getClock(23, 59, 59);

        assertThat(c.toString()).isEqualTo("O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY\n");
    }

    @Test
    public void clock_now() throws Exception {

        Clock c = clockService.now();

        assertThat(c.toString()).isNotEmpty();
    }
}
