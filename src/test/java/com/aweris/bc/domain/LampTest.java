package com.aweris.bc.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static com.aweris.bc.domain.Color.RED;
import static com.aweris.bc.domain.Color.YELLOW;
import static com.aweris.bc.domain.Status.OFF;
import static com.aweris.bc.domain.Status.ON;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class LampTest {

    @Test
    public void off_lamp_to_string() throws Exception {
        Lamp l = new Lamp(OFF, RED);

        assertThat(l.toString()).isEqualTo("O");
    }

    @Test
    public void off_lamp_change_status_to_string() throws Exception {
        Lamp l = new Lamp(OFF, RED);

        assertThat(l.toString()).isEqualTo("O");

        l.changeStatus(ON);

        assertThat(l.toString()).isEqualTo("R");
    }

    @Test
    public void on_lamp_red_to_string() throws Exception {
        Lamp l = new Lamp(ON, RED);

        assertThat(l.toString()).isEqualTo("R");
    }

    @Test
    public void on_lamp_yellow_to_string() throws Exception {
        Lamp l = new Lamp(ON, YELLOW);

        assertThat(l.toString()).isEqualTo("Y");
    }
}
