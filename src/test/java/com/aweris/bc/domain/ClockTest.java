package com.aweris.bc.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ClockTest {

    @Test
    public void clock_0_hour_0_minute_0_second() throws Exception {

        LocalTime time = LocalTime.of(0, 0, 0);

        Clock c = new Clock(time);

        assertThat(c.toString()).isEqualTo("Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO\n");
    }

    @Test
    public void clock_17_hour_36_minute_5_second() throws Exception {

        LocalTime time = LocalTime.of(17, 36, 5);

        Clock c = new Clock(time);

        assertThat(c.toString()).isEqualTo("O\nRRRO\nRROO\nYYRYYRYOOOO\nYOOO\n");
    }

    @Test
    public void clock_23_hour_59_minute_59_second() throws Exception {

        LocalTime time = LocalTime.of(23, 59, 59);

        Clock c = new Clock(time);

        assertThat(c.toString()).isEqualTo("O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY\n");
    }
}
