package com.aweris.bc.domain;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static com.aweris.bc.domain.Color.RED;
import static com.aweris.bc.domain.Color.YELLOW;
import static com.aweris.bc.domain.Status.OFF;
import static com.aweris.bc.domain.Status.ON;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class RowTest {

    @Test
    public void all_off_row() throws Exception {
        Lamp l1 = new Lamp(OFF, RED);
        Lamp l2 = new Lamp(OFF, RED);
        Lamp l3 = new Lamp(OFF, RED);
        Lamp l4 = new Lamp(OFF, RED);

        Row row = new Row(l1, l2, l3, l4);

        assertThat(row.toString()).isEqualTo("OOOO");
    }

    @Test
    public void single_lamp_on_at_row() throws Exception {
        Lamp l1 = new Lamp(ON, RED);
        Lamp l2 = new Lamp(OFF, RED);
        Lamp l3 = new Lamp(OFF, RED);
        Lamp l4 = new Lamp(OFF, RED);

        Row row = new Row(l1, l2, l3, l4);

        assertThat(row.toString()).isEqualTo("ROOO");
    }

    @Test
    public void four_lamp_on_at_row() throws Exception {
        Lamp l1 = new Lamp(ON, YELLOW);
        Lamp l2 = new Lamp(ON, YELLOW);
        Lamp l3 = new Lamp(ON, RED);
        Lamp l4 = new Lamp(ON, YELLOW);
        Lamp l5 = new Lamp(OFF, YELLOW);
        Lamp l6 = new Lamp(OFF, RED);
        Lamp l7 = new Lamp(OFF, YELLOW);
        Lamp l8 = new Lamp(OFF, YELLOW);
        Lamp l9 = new Lamp(OFF, RED);
        Lamp l10 = new Lamp(OFF, YELLOW);
        Lamp l11 = new Lamp(OFF, YELLOW);

        Row row = new Row(l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11);

        assertThat(row.toString()).isEqualTo("YYRYOOOOOOO");
    }

}
