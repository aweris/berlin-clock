package com.aweris.bc.rest;

import com.aweris.bc.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class ClockRestControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

    }

    @Test
    public void getTimeNow() throws Exception {
        mockMvc.perform(get("/bc/api/time/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getTime_0_hour_0_minute_0_second() throws Exception {
        mockMvc.perform(get("/bc/api/time/0/0/0").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"time\":\"00:00:00\", \"result\":\"Y\\nOOOO\\nOOOO\\nOOOOOOOOOOO\\nOOOO\\n\"}"));
    }

    @Test
    public void getTime_12_hour_10_minute_0_second() throws Exception {
        mockMvc.perform(get("/bc/api/time/12/10/0").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"time\":\"12:10:00\", \"result\":\"Y\\nRROO\\nRROO\\nYYOOOOOOOOO\\nOOOO\\n\"}"));
    }

    @Test
    public void error_invalid_hour() throws Exception {
        mockMvc.perform(get("/bc/api/time/123/0/0").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Invalid value for HourOfDay (valid values 0 - 23): 123"));
    }

    @Test
    public void error_with_invalid_minute() throws Exception {
        mockMvc.perform(get("/bc/api/time/0/-1/0").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Invalid value for MinuteOfHour (valid values 0 - 59): -1"));
    }

    @Test
    public void error_with_invalid_second() throws Exception {
        mockMvc.perform(get("/bc/api/time/0/0/123").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Invalid value for SecondOfMinute (valid values 0 - 59): 123"));
    }

}
