package com.aweris.bc;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    @Autowired
    ApplicationContext ctx;

    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Test
    public void loaded_custom_logback_config() throws Exception {
        Application.main(new String[0]);

        this.outputCapture.expect(containsString("Berlin Clock is up!!!"));
    }

    @Test
    public void context_load_should_not_be_null() throws Exception {
        assertThat(this.ctx).isNotNull();
    }
}
