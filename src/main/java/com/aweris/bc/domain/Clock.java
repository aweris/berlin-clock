package com.aweris.bc.domain;


import java.time.LocalTime;

import static com.aweris.bc.util.ClockUtils.*;

public class Clock {

    private LocalTime localTime;

    /*indicates seconds*/
    private Lamp second;

    /* indicates 5 hour*/
    private Row fiveHours;

    /* indicates 1 hour*/
    private Row oneHourRow;

    /* indicates 5 minute*/
    private Row fiveMins;

    /* indicates 1 minute*/
    private Row oneMins;

    public Clock(LocalTime time) {
        this.localTime = time;
        this.second = secondLamp(time);
        this.fiveHours = fiveHourRow(time);
        this.oneHourRow = oneHourRow(time);
        this.fiveMins = fiveMinuteRow(time);
        this.oneMins = oneMinuteRow(time);
    }

    public LocalTime getLocalTime() {
        return localTime;
    }

    public Lamp getSecond() {
        return second;
    }

    public Row getFiveHours() {
        return fiveHours;
    }

    public Row getOneHourRow() {
        return oneHourRow;
    }

    public Row getFiveMins() {
        return fiveMins;
    }

    public Row getOneMins() {
        return oneMins;
    }

    @Override
    public String toString() {

        return String.valueOf(second) + "\n" + fiveHours + "\n" + oneHourRow + "\n" + fiveMins + "\n" + oneMins + "\n";
    }
}
