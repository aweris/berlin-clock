package com.aweris.bc.domain;

public enum Color {
    RED("R"), YELLOW("Y");

    private String key;

    Color(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
