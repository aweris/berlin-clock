package com.aweris.bc.domain;


import java.util.Arrays;
import java.util.List;

public class Row {

    private List<Lamp> lamps;

    public Row(Lamp... lamps) {
        this.lamps = Arrays.asList(lamps);
    }

    public List getLamps() {
        return lamps;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        lamps.stream().forEach(sb::append);

        return sb.toString();
    }
}
