package com.aweris.bc.domain;

public class Lamp {

    private Status status;
    private Color color;

    public Lamp() {
        this.status = Status.OFF;
    }

    public Lamp(Status status, Color color) {
        this.status = status;
        this.color = color;
    }

    public void changeStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return status.equals(Status.OFF) ? "O" : color.getKey();
    }
}
