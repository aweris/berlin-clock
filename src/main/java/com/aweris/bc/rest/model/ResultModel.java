package com.aweris.bc.rest.model;

import com.aweris.bc.domain.Clock;

import java.time.format.DateTimeFormatter;

public class ResultModel {

    private String time;
    private String result;

    public ResultModel(Clock clock) {
        this.time = clock.getLocalTime().format(DateTimeFormatter.ISO_LOCAL_TIME);
        this.result = clock.toString();
    }

    public String getTime() {
        return time;
    }

    public String getResult() {
        return result;
    }
}
