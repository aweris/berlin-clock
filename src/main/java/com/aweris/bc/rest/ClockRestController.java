package com.aweris.bc.rest;

import com.aweris.bc.rest.model.ResultModel;
import com.aweris.bc.service.ClockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bc/api/time")
public class ClockRestController {


    @Autowired
    private ClockService clockService;

    @RequestMapping(method = RequestMethod.GET)
    private ResponseEntity<ResultModel> getNow() {
        return ResponseEntity.ok(new ResultModel(clockService.now()));
    }

    @RequestMapping(value = "/{hour}/{min}/{sec}", method = RequestMethod.GET)
    private ResponseEntity getTime(@PathVariable int hour, @PathVariable int min, @PathVariable int sec) {
        return ResponseEntity.ok(new ResultModel(clockService.getClock(hour, min, sec)));
    }

}
