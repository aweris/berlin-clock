package com.aweris.bc.util;

import com.aweris.bc.domain.Lamp;
import com.aweris.bc.domain.Row;

import java.time.LocalTime;
import java.util.stream.IntStream;

import static com.aweris.bc.domain.Color.RED;
import static com.aweris.bc.domain.Color.YELLOW;
import static com.aweris.bc.domain.Status.ON;

/**
 * Util class for initializing clock.
 */
public class ClockUtils {

    public static Lamp secondLamp(LocalTime time) {
        return time.getSecond() % 2 == 0 ? new Lamp(ON, YELLOW) : new Lamp();
    }

    public static Row fiveHourRow(LocalTime time) {
        int range = time.getHour() / 5;

        Lamp[] lamps = new Lamp[4];

        IntStream.range(0, range).forEachOrdered(i -> lamps[i] = new Lamp(ON, RED));
        IntStream.range(range, 4).forEachOrdered(i -> lamps[i] = new Lamp());

        return new Row(lamps);
    }

    public static Row oneHourRow(LocalTime time) {
        int range = time.getHour() % 5;

        Lamp[] lamps = new Lamp[4];

        IntStream.range(0, range).forEachOrdered(i -> lamps[i] = new Lamp(ON, RED));
        IntStream.range(range, 4).forEachOrdered(i -> lamps[i] = new Lamp());

        return new Row(lamps);
    }

    public static Row fiveMinuteRow(LocalTime time) {
        int range = time.getMinute() / 5;

        Lamp[] lamps = new Lamp[11];

        IntStream.range(0, range).forEachOrdered(i -> lamps[i] = new Lamp(ON, ((i + 1) % 3) == 0 ? RED : YELLOW));
        IntStream.range(range, 11).forEachOrdered(i -> lamps[i] = new Lamp());

        return new Row(lamps);
    }

    public static Row oneMinuteRow(LocalTime time) {
        int range = time.getMinute() % 5;

        Lamp[] lamps = new Lamp[4];

        IntStream.range(0, range).forEachOrdered(i -> lamps[i] = new Lamp(ON, YELLOW));
        IntStream.range(range, 4).forEachOrdered(i -> lamps[i] = new Lamp());

        return new Row(lamps);
    }
}
