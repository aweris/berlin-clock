package com.aweris.bc.service;

import com.aweris.bc.domain.Clock;

import java.time.LocalTime;

public interface ClockService {

    Clock getClock(int hour, int min, int second);

    Clock getClock(LocalTime time);

    Clock now();
}
