package com.aweris.bc.service.impl;

import com.aweris.bc.domain.Clock;
import com.aweris.bc.service.ClockService;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class ClockServiceImpl implements ClockService {

    @Override
    public Clock getClock(int hour, int min, int second) {
        return new Clock(LocalTime.of(hour, min, second));
    }

    @Override
    public Clock getClock(LocalTime time) {
        return new Clock(time);
    }

    @Override
    public Clock now() {
        return new Clock(LocalTime.now());
    }
}
