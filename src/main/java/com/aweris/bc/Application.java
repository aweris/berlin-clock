package com.aweris.bc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    @PostConstruct
    private void init() {
        LOGGER.info("Berlin Clock is up!!!");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
