# BERLIN CLOCK
Representation of the Berlin Clock as Service

## Running Project
Prefered way to run application is use Make command

- For building project :
```
make build
```
- For running project :
```
make run
```
 if you don't prefer use make xommand, you can run mvn and java commands manually
- For building project :
```
mvn clean package spring-boot:build-info spring-boot:repackage
```
- For running project :
```
java -jar target/berlin-clock-1.0-SNAPSHOT-exec.jar
```

------------------------------

## Running Project with Docker
- For building project with docker:
```
make docker-build
```
- For building running with docker:
```
make docker-run
```

PS: Run ip might change according to your docker configuration. if you'r not able to access from localhost , please check you docker ip and and use it as application ip

------------------------------
## Running Project with Vagrant
Vagrant provision simulates production environment for our local test. In this config application servers , 8080 ports , redirecting to loadbalancers , 80 port .  Vagrant config has :
- 1 haproxy node (172.17.4.50) - haproxy portal *:9000 username :admin password : admin
- 1 Service Discovery (172.17.4.101) - core-os
- 3 Worker node (172.17.4.201/202/203) - application servers

#### Prerequisites
 - Vagrant (v1.6+)
 - VirtualBox

For starting vagrant run
```
cd devops/vagrant/ && ./init.sh
```
For destroy vagrant config
```
 cd devops/vagrant && vagrant destroy -f
```
in this setup you can ftest Load Balancing, Fail Over etc.

## Testing Application
Generally application is accessable from localhost. At Docker container or Vagrant provision you need to use their ip.

### Application Heath Check
returns application health status
- uri : ```${APP_IP}:9990/management/health```
- method : GET

### Berlin Time
 Returns current time at berlin clock format, json model contains requested time (in ```"hh:mm:ss"``` format) and result string seperated with new lines. For String representation O = OFF, Y = YELLOW, R = RED
 
#### live
- uri : ```${APP_IP}:8080/bc/api/time```
- method : GET
- return format : ```{"time":"00:00:00", "result":"Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO\n"}"}```

#### time with hour , minute, second
- uri : ```${APP_IP}:8080/bc/api/time/{hour}/{minute}/{second}```
- method : GET
- return format : ```{"time":"00:00:00", "result":"Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO\n"}"}```

######  ps : for vagrant provision uri formats using port 80 like  ```172.17.4.50/bc/api/time``