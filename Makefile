# Variable for re-use in the file
DOCKER_REGISTRY=registry.gitlab.com
IMAGE_NAME=aweris/berlin-clock
JAR=target/berlin-clock-1.0-SNAPSHOT-exec.jar

# Building fat jar
build:
	mvn clean package spring-boot:build-info spring-boot:repackage

# Running fat jar
run:
	java -jar ${JAR}

# Building your custom docker image
docker-build:
	mvn clean package spring-boot:build-info spring-boot:repackage && docker build -t ${DOCKER_REGISTRY}/${IMAGE_NAME} .

# Running your custom-built docker image locally
docker-run:
	docker run --rm -p 8080:8080 -p 9990:9990 -ti --name berlin-clock-container ${DOCKER_REGISTRY}/${IMAGE_NAME}

# To remove the stuff we built locally afterwards
clean:
	docker rmi -f ${DOCKER_REGISTRY}/${IMAGE_NAME}