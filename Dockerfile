FROM anapsix/alpine-java:8

MAINTAINER Ali AKCA "ali.aakca@gmail.com"

# Get rid of the debconf messages
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /code

ADD target/berlin-clock-1.0-SNAPSHOT-exec.jar  /code/berlin-clock-1.0-SNAPSHOT-exec.jar

EXPOSE 8080
EXPOSE 9990

ENTRYPOINT ["/opt/jdk/jre/bin/java", "-jar", "/code/berlin-clock-1.0-SNAPSHOT-exec.jar"]